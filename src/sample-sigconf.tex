% !TeX root = ./sample-sigconf.tex
% !TEX TS-program = pdflatex
\documentclass[10pt,sigconf]{acmart}

\usepackage{booktabs} % For formal tables

\usepackage{color}

\definecolor{pblue}{rgb}{0.13,0.13,1}
\definecolor{pgreen}{rgb}{0,0.5,0}
\definecolor{pred}{rgb}{0.9,0,0}
\definecolor{pgrey}{rgb}{0.46,0.45,0.48}

\usepackage{listings}
\lstset{
  language=Java,
  aboveskip=0mm,
  belowskip=0mm,
  showspaces=false,
  showtabs=false,
  breaklines=true,
  showstringspaces=false,
  breakatwhitespace=true,
  commentstyle=\color{pgreen},
  keywordstyle=\color{pblue},
  stringstyle=\color{pred},
  basicstyle=\ttfamily
}

\graphicspath{{figure/}{figures/}}

\setcopyright{none}

\acmDOI{10.475/123_4}

\acmISBN{123-4567-24-567/08/06}

%Conference
\acmConference[NANOCOM'21]{ACM}{September 2021}{Catania, Italy}
\acmYear{2021}
\copyrightyear{2021}
\acmPrice{15.00}


\begin{document}
\title{Hop Count Routing: A Routing Algorithm for Resource-Constrained, Identity-Free Medical Nanonetworks}

\author{Christopher Starck}
% \authornote{Christopher Starck}
\orcid{1234-1234-1234}
\affiliation{%
  \institution{University of Lübeck}
  \city{Lübeck}
  \state{Germany}
}
\email{christopher.starck@uni-luebeck.de}
% The default list of authors is too long for headers}
\renewcommand{\shortauthors}{Christopher Starck}

\begin{abstract}

Nanodevices are tiny robots operating within a human body.
They may help to detect and treat many kinds of diseases.
In a resource-constrained environment, they need to work together.
Communication is essential to enable this collaboration.
Routing algorithms focus on enabling communication inside a nanonetwork.
The authors propose a novel routing algorithm based on a network topology constructed from the hop count distance to a single gateway.
It exploits the distance as a direction indicator to deliver data from or to the gateway.
\end{abstract}

% http://dl.acm.org/ccs.cfm
\begin{CCSXML}
  <ccs2012>
     <concept>
         <concept_id>10010405.10010444</concept_id>
         <concept_desc>Applied computing~Life and medical sciences</concept_desc>
         <concept_significance>500</concept_significance>
         </concept>
     <concept>
         <concept_id>10003033.10003068</concept_id>
         <concept_desc>Networks~Network algorithms</concept_desc>
         <concept_significance>500</concept_significance>
         </concept>
     <concept>
         <concept_id>10003033.10003039.10003045.10003046</concept_id>
         <concept_desc>Networks~Routing protocols</concept_desc>
         <concept_significance>500</concept_significance>
         </concept>
     <concept>
         <concept_id>10003033.10003106.10010582.10011668</concept_id>
         <concept_desc>Networks~Mobile ad hoc networks</concept_desc>
         <concept_significance>500</concept_significance>
         </concept>
     <concept>
         <concept_id>10010583.10010588.10011670</concept_id>
         <concept_desc>Wireless access points, base stations and infrastructure</concept_desc>
         <concept_significance>100</concept_significance>
         </concept>
   </ccs2012>
\end{CCSXML}

\ccsdesc[500]{Applied computing~Life and medical sciences}
\ccsdesc[500]{Networks~Network algorithms}
\ccsdesc[500]{Networks~Routing protocols}
\ccsdesc[500]{Networks~Mobile ad hoc networks}
\ccsdesc[100]{Wireless access points, base stations, and infrastructure}

\keywords{Routing; Nanonetworks; Algorithm; Hop Count; Identity-Free}

\maketitle

\begin{figure}[b]
  \centering
  \includegraphics[width=\linewidth]{1}
  \caption{Example of a 3-tiered medical nanonetwork.}
  \label{fig:1}
\end{figure}

\section{Introduction}

Current medical challenges involve processes at a molecular scale.
Nanodevices pose a possible solution to such problems at a cellular or sub-cellular scale.
Following the definition from \cite{formalDefNanoRobots}, nanodevices are artificial constructs with a nanoscale size.
Nanodevices have predefined functions and work in specific environments, such as the bloodstream.
A nanodevice consists of mandatory components and zero or more optional components.
Components may vary from environment to environment, but mandatory components always include a power supply.

Nanonodes are nanodevices collaborating in networks, for example in Body Area Networks (BAN) or the Internet of Nano-Things (IoNT).
They need to work together, as a consequence of size, memory, and other problem-specific constraints.
Storage of complex routing tables or unique identifiers \cite{CompReqs} and computation of forwarding decisions may not be possible.
Medical nanonetworks (see fig. \ref{fig:1}) are composed of nanonodes and are deployed within the human body.
% The original paper presents a directional message propagation algorithm based on distance measured by hop count.

A microscale gateway attached to or implanted in the skin connects to a regular computer in the BAN.
The micro-gateway mediates BAN communication via Bluetooth or terahertz waves.
The medical nanonetwork exhibits two major flows of information:
\begin{enumerate}
  \item Sensor information towards the micro-gateway,
  \item Actuation commands from the micro-gateway into the network.
\end{enumerate}
Nanonodes are nearly always in motion.
They either operate in the bloodstream or are attached to tissue which moves with the person.
Consequently, there can be only spontaneous peer-to-peer connections.

\section{Hop Count Routing}

The shape of medical nanonetworks indicates that data flows only from or towards the micro-gateway.
The hop count refers to how many times a message is forwarded.
To determine the direction from and to the micro-gateway, we consider the hop count of a nanonode.
Figure \ref{fig:2} shows that lower hop counts are closer to the micro-gateway and larger hop counts are further away.

\begin{figure}[t]
  \centering
  \includegraphics[width=\linewidth]{2}
  \caption{
    The gateway-oriented topology.
    Each node shows its assigned hop count.
    Lower hop counts are closer to the gateway and project a brighter communication area.
  }
  \label{fig:2}
\end{figure}

\subsection{Model Assumptions}

Despite not considering real-world conditions, the following assumptions provide insights into algorithm performance and practical feasibility.

\begin{itemize}
  \item Nanonodes have infinite energy.
  \item Nanonodes do not degrade and do not move.
  \item Communication is free of interference and loss.
\end{itemize}

\subsection{Algorithm}

\begin{lstlisting}[language=Java, caption=Hop Count Propagation Algorithm,label={lst:1}]
  onReceiveMsg(PropagrationMessage m) {
    if (m.hopCount + 1 < this.hopCount) {
      this.hopCount = m.hopCount + 1;
      sendMessage(
        PropagrationType, this.hopCount
      );
  }}
\end{lstlisting}

A nanonode keeps track of its hop count $k$, with an initial value of $k=\infty$.
A message has a type, hop count $k_m$, and some data.
When a propagation message is received, the local and the message's hop counts are compared.
If the message's hop count is lower than the nodes, the local hop count becomes $k = k_m+1$.

Algorithm \ref{lst:1} requires an updated hop count for each nanonode.
Hop count values for each nanonode update themselves after an initial propagation message.
Immediate neighbors of the micro-gateway receive the message and set their hop counts to 1.
Then, the neighbors pass the message onto their neighbors.
Eventually, all nanonodes have an updated hop count.

\begin{lstlisting}[language=Java, caption=Sensor Data Retrieval Algorithm,label={lst:2}]
onReceiveMsg(SensorMessage m) {
  if (this.hopCount + 1 < m.hopCount) {
    sendMessage(
      SensorType, this.hopCount, m.data
    );
}}
\end{lstlisting}

\subsection{Complexity and Runtime}

\subsubsection{Hop Count Propagation} \hfill

In the worst case, a nanonode sends a message to all other $N$ nanonodes in the network.
If this happens for all nanonodes, the network sends $O(N^2)$ messages.

\subsubsection{Sensor Data Retrieval} \hfill

In the worst case, a nanonode with hop count $k$ forwards all messages with hop count $k+1$.
If this happens for all nanonodes $N_k$ with hop count $k$, the network sends $O(N_k \cdot N_{k+1})$x.
$N_k$ can be approximated as $\sum_{i=1}^{K}N_i \leq N^K$.
Thus, $O(N^K)$ messages are sent.

% \subsection{Optimisation: Destructive Retrieval}

% The authors propose the following change to the algorithm \ref{lst:2}:
% After a nanonode forwards a message, the local hop count resets to $\infty$.
% Now, each nanonode forwards the sensor data once.
% As a result, at most $O(N)$ messages are sent.
% After each message, hop count values for all nanonodes need to be updated again.
% Thus, $O(N+N^2)$ messages are sent.

\section{Conclusion}
In a severely resource-constrained environment, nanonodes cannot carry unique identifiers or store and maintain routing tables.
Due to limited transmission ranges, nanonodes relay messages in a multi-hop fashion.
The authors present a novel routing for medical nanonetworks using a topology based on the hop count distance from the gateway node.
Nodes determine their hop count with a propagation phase.
The hop count is equal to the number of hops the message has taken so far.
The topology allows to route messages towards the gateway without any additional information.

\nocite{originalPaper}
\bibliographystyle{acm}
\bibliography{sigproc}

\end{document}