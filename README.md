# Informationen zum Kurs S4518-S (Seminar, 2 SWS)

## CS4518-S: Aktuelle und zukünftige Netzwerktechnologien 
Das Seminar (siehe [Modulhandbuch](https://www.uni-luebeck.de/index.php?id=8867&tx_webparser_pi1[modulid]=2370) (TODO)) wird dieses Jahr zum ersten Mal angeboten. Der Inhalt dieser Veranstaltung orientiert sich an den neuesten Forschungsthemen im Bereich der Netzwerkforschung und den dort angewandten, wissenschaftlichen Methoden.

Wir werden versuchen die Vorlesungen **vollständig O-Sync** stattfinden zu lassen. Hierfür wird es zu Beginn eine entsprechende Umfrage geben. Der erste Termin wird am 15.04.2021 stattfinden, alles Weitere wird dort besprochen.

## Wichtige Infos im Vorweg

-    Dozent ist Dr. rer. nat. Florian Lau (TODO) und Modulverantwortlicher ist Prof. Dr. rer. nat. Stefan Fischer (TODO)
-    Zeit und Ort: Mo 14:15 - 15:45, C4-S02 - 058.400 (oder online, wenn zu viele Personen teilnehmen - mögliche Änderungen werden im Semester bekannt gegeben)
-    Erster Termin ist der 15.04.2021
-    Seminar, 2 SWS, Sprache Deutsch
-    Link zum Univis (TODO)
-    Diese Veranstaltung ist Teil eines wissenschaftlich orientierten Moduls. 
-    Diese Veranstaltung ist neu, es kann also sein, dass sich hier und da Rechtschreibfehler eingeschlichen haben!

## Ablauf

1. Vorbesprechung + Einrichtungsguide + Download aller Unterlagen via Git
2. 1. Meilenstein: Entwicklungumgebung einrichten & Paper raussuchen (entweder Nanothema (siehe Git) oder selbst rausgesucht). Beinhaltet Zotero, Git, Visual Studio Code, Grammarly Addon, Google Translate Addon und das LaTeX Workshop Addon.
3. 2. Meilenstein: Paper Vorlage der Nanocom 2021 selbstständig heraussuchen & lauffähig machen.
4. 3. Meilenstein: Leitfäden Paper & Vortrag lesen & verstehen. Beginn des Schreibprozesses.
5. 4. Meilenstein: Abgabe Shortpaper & Präsentation.
6. 5. Meilenstein: Vortragsprüfung pro Person 15 Minuten.

## Modulinformationen

-    Master Medieninformatik 2020 (Wahlpflicht), Informatik, beliebiges Fachsemester
-    Master Medizinische Informatik 2019 (Wahlpflicht), eHealth / Informatik, 1. oder 2. Fachsemester
-    Als Teil des Moduls CS4518-KP12: (CS5158-V+Ü = SS, CS5161-V+P = WS, CS4518-S= WS)
-    Master Robotics and Autonomous Systems 2019 (Vertiefungsmodul), Vertiefung, 1. oder 2. Fachsemester
-    Master IT-Sicherheit 2019 (Wahlpflicht), Vertiefung Informatik, 1. oder 2. Fachsemester
-    Master Entrepreneurship in digitalen Technologien 2020 (Wahlpflicht), Technologiefach Informatik, 2. oder 3. Fachsemester
-    Master Informatik 2019 (Wahlpflicht), Vertiefungsmodule, beliebiges Fachsemester

## Thematische Voraussetzungen

-    Kenntnisse zu Computernetzen
-    Grundlagen der Theoretischen Informatik
-    Biologische Grundlagen DNA
-    Grundlagen Windows
-    Grundlagen Programmieren
-    Grundlagen Wissenschaftliches Arbeiten

## Downloads

-   Git für Windows ([Gitbash](https://gitforwindows.org/))
-   [Visual Studio Code](https://code.visualstudio.com/)
-   [Zotero](https://www.zotero.org/) Bibliotheksverwaltungssoftware
-   [Grammarly Addon](https://marketplace.visualstudio.com/items?itemName=znck.grammarly) für Visual Studio Code
-   [Google Translate](https://marketplace.visualstudio.com/items?itemName=funkyremi.vscode-google-translate) Addon für Visual Studio Code
-   [LaTeX Workshop Addon](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) für Visual Studio Code

### Magic Bytes Paper
```tex    
    % !TEX program = pdflatex
    % !BIB pro%gram = bibtex
    % !TEX encoding = UTF-8 Unicode
    % !TEX spellcheck = en_us
```
### Magic Bytes Presentation:
```tex
    % !TEX program = xelatex
    % !TEX encoding = UTF-8 Unicode
    % !TEX spellcheck = en_us
```

## Zeitplan

Wird Im Detail mit den Studierenden besprochen.


